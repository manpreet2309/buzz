import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FizzBuzzTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
//R1 divisible by 3
@Test
	public void testDivisibleBy3() {
		FizzBuzz fb = new FizzBuzz();
		String result = fb.buzz(27);
		assertEquals("fizz",result);
		
	}
	//R2 divisible by 5
		@Test
		public void testDivisibleBy5() {
			FizzBuzz fb = new FizzBuzz();
			String result = fb.buzz(10);
			assertEquals("buzz",result);
			
		}
		//R3 divisible by 3 & 5, return "fizzbuzz"
				@Test
				public void testDivisibleBy3and5() {
					FizzBuzz fb = new FizzBuzz();
					String result = fb.buzz(15);
					assertEquals("fizzbuzz",result);
					
				}
				//R4 if no other  requirement fulfilled, return the number
				@Test
				public void testOtherNumber() {
					FizzBuzz fb = new FizzBuzz();
					String result = fb.buzz(4);
					assertEquals("4",result);
					
				}
				//R5
				@Test
				public void testPrimeNumber() {
					FizzBuzz fb = new FizzBuzz();
					String result = fb.buzz(11);
					assertEquals("whizz",result);
					
				}
				//R6 if number is prime + divisible by 3
				@Test
				public void testAppendwhizz() {
					FizzBuzz fb = new FizzBuzz();
					String result = fb.buzz(3);
					assertEquals("fizzwhizz",result);
					
					result = fb.buzz(5);
					assertEquals("buzzwhizz",result);
					
				}
}
